import DropzoneS3Uploader from 'react-dropzone-s3-uploader'
import React, { Component } from 'react'
import UploadDisplay from'./UploadDisplay';
export default class S3Uploader extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            maxSize:32000000,
            acceptedFiles: null,
          };
    }
  handleFinishedUpload = info => {
    console.log('File uploaded with filename', info.filename)
    console.log('Access it on s3 at', info.fileUrl)
  }

  render() {
    const uploadOptions = {
      server: 'http://localhost:4000',
      s3Url: 'https://diskava-static.s3.amazonaws.com/',
      signingUrlQueryParams: {uploadType: 'audio'},
    }

    return (
        <div>
      <DropzoneS3Uploader 
        onFinish={this.handleFinishedUpload} 
        maxSize={this.state.maxSize}
        upload={uploadOptions}
        s3Url = {uploadOptions.s3Url}
      >
      <UploadDisplay />
      </DropzoneS3Uploader>
      </div>
    )
  }
}