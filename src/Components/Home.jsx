import React, { Component } from "react";
import logo from '../logo.svg';
import { Link } from 'react-router-dom';
class Home extends Component {

  constructor(props) {
    super(props);
    this.state = { stylePath: "page1.css" };
  }
  render() {
    return (
      <section className="hero is-fullheight">
      <link rel="stylesheet" type="text/css" href={this.state.stylePath} />
        <div className="hero-head">
          <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
              <div className="navbar-item">
                <p>Diskava</p>
              </div>
            </div>

            <div className="navbar-end">
              <div className="navbar-item">
                <div className="buttons">
                <p>
                <Link to="/news">  
                    
                    TimeLine
                  
                      </Link>
                      </p>
                      <p>
                      <Link to="/time">  
                    
                    Gantt
                  
                      </Link>
                      </p>
                </div>
              </div>
            </div>
          </nav>
        </div>
         <div classNameName="App">
         <img src={logo} className="App-logo" alt="logo" />
        <h1> Welcome to React </h1>
        
     
      </div>
      </section>
    );
  }
}

export default Home;
