
import React, { Component } from 'react';

import ReactDOM from "react-dom";

import TimeLine from "react-gantt-timeline";

// end should be Date + track length 
Date.prototype.addDays = function(days) {
  var date = new Date(this.valueOf());
  date.setDate(date.getDate() + days);
  return date;
}

let dateorg= new Date();
let e1 =dateorg.addDays(5);
let s2 =dateorg.addDays(15);
let e2 =dateorg.addDays(25);

let data=[ {id:1,start:dateorg, end:e1 ,name:'Track 1' , color:"red"}, {id:2,start:s2, end:e2 ,name:'Track 2'}];
let links=[ {id:1,start:1, end:2}, {id:2,start:1, end:3}];
export default class Gantt extends React.Component {
    constructor(props) {
        super(props);
        let d1 = new Date();
        let d2 = new Date();
        d2.setDate(d2.getDate() + 5);
        let d3 = new Date();
        d3.setDate(d3.getDate() + 8);
        let d4 = new Date();
        d4.setDate(d4.getDate() + 20);
        this.state={
            data:[],
            links:[]
        };
        this.data = [
          {
            id: 1,
            start: d1,
            end: d2,
            name: "Demo Task 1"
          },
          {
            id: 2,
            start: d3,
            end: d4,
            name: "Demo Task 2",
            color: "orange"
          }
        ];
        this.links = [{ id: 1, start: 1, end: 2 }];
      }

      createLink(start, end) {
        return {
          id: this.genID(),
          start: start.task.id,
          startPosition: start.position,
          end: end.task.id,
          endPosition: end.position
        };
      }
      onUpdateTask = (item, props) => {
        item.start = props.start;
        item.end = props.end;
        this.setState({ data: [...this.state.data] });
      };
      onCreateLink = item => {
        let newLink = this.createLink(item.start, item.end);
        this.setState({ links: [...this.state.links, newLink] });
      };

  render() {
 
    return (
        <div>
      <h1> Gantt </h1>
      <TimeLine
            data={data}
            links={links}
            onUpdateTask={this.onUpdateTask}
            onCreateLink={this.onCreateLink}
          />
      </div>
    )
  }
}